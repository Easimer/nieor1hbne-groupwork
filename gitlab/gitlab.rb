external_url 'http://git.szabomeszaros.hu'

gitlab_rails['ldap_enabled'] = true
gitlab_rails['ldap_servers'] = {
  'main' => {
    'label' => 'ad.szabomeszaros.hu',

    'host' => 'ad.szabomeszaros.hu',
    'port' => 389,
    'encryption' => 'plain',

    'bind_dn' => 'CN=admin, DC=szabomeszaros, DC=hu',
    'password' => 'sis123',

    'base' => 'OU=Users, DC=szabomeszaros, DC=hu',

    'uid' => 'uid'
  }
}
