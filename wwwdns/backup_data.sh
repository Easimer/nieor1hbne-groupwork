#!/bin/bash

DESTINATION_FILE=/dev/null

function AttachDestination() {
    echo "[==] Mounting backup destination"
}

function ReleaseDestination() {
    echo "[==] Releasing backup destination"
}

AttachDestination
tar czf $DESTINATION_FILE /var/www/html/
ReleaseDestination
