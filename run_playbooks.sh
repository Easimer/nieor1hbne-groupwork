#!/bin/bash

ansible-playbook -i hosts gw/gw.yml
ansible-playbook -i hosts wwwdns/wwwdns.yml
ansible-playbook -i hosts gitlab/gitlab.yml
